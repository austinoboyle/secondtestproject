# README #

Test project to get us going with git/bitbucket

### What is this repository for? ###

Making a pseudo-login website and registering users in a database

### How do I get set up? ###

git clone https://bitbucket.org/austinoboyle/secondtestproject.git
(enter user/password if required)
cd secondtestproject
git checkout development
git checkout -b (your-feature)
Start coding!

### Contribution guidelines ###

Use the workflow outlined in http://nvie.com/posts/a-successful-git-branching-model/

### Who do I talk to? ###

